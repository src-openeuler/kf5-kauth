%undefine __cmake_in_source_build
%global framework kauth

Name:           kf5-%{framework}
Version:        5.116.0
Release:        1
Summary:        KDE Frameworks 5 Tier 2 integration module to perform actions as privileged user

License:        BSD-3-Clause AND CC0-1.0 AND LGPL-2.0-or-later AND LGPL-2.1-or-later
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-rpm-macros

BuildRequires:  polkit-qt5-1-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qttools-devel

%description
KAuth is a framework to let applications perform actions as a privileged user.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       kf5-kcoreaddons-devel >= %{majmin}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{version} -p1

%build
%cmake_kf5 \
  -DKDE_INSTALL_LIBEXECDIR=%{_kf5_libexecdir}

%cmake_build


%install
%cmake_install

%find_lang_kf5 kauth5_qt


%ldconfig_scriptlets

%files -f kauth5_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_libdir}/libKF5Auth.so.5*
%{_kf5_libdir}/libKF5AuthCore.so.5*
%{_kf5_datadir}/dbus-1/system.d/org.kde.kf5auth.conf
%{_kf5_qtplugindir}/kauth/
%{_kf5_datadir}/kf5/kauth/
%{_kf5_libexecdir}/kauth/

%files devel
%{_kf5_includedir}/KAuth/
%{_kf5_includedir}/KAuthCore/
%{_kf5_includedir}/KAuthWidgets/
%{_kf5_libdir}/libKF5Auth.so
%{_kf5_libdir}/libKF5AuthCore.so
%{_kf5_libdir}/cmake/KF5Auth/
%{_kf5_archdatadir}/mkspecs/modules/qt_KAuth*.pri


%changelog
* Thu Jan 16 2025 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- Update package to version 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 04 2024 huayadong <huayadong@kylinos.cn> - 5.115.0-1
- Update package to version 5.115.0

* Thu Jan 04 2024 wangqia <wangqia@uniontech.com> - 5.113.0-1
- Update to upstream version 5.113.0

* Thu Aug 03 2023 haomimi <haomimi@uniontech.com> - 5.108.0-1
- update verison to 5.108.0

* Mon Dec 12 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 5.100.0-1
- Update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Mon Jul 04 2022 loong_C <loong_c@yeah.net> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 tanyulong <tanyulong@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Thu Jan 13 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Dec 13 2021 weidong<weidong@uniontech.com> - 5.55.0-2
- Delete dist macro

* Thu Jul 23 2020 wangmian <wangmian@kylinos.cn> - 5.55.0-1
- Init kf5-kauth project
